import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './routes'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
// import 'vuetify/dist/vuetify.min.css';
import { getData, utils } from './func'

// Vue.use(Vuetify)
Vue.use(VueRouter)

const router = new VueRouter({
  routes: Routes,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  mode: 'history'
});

export const bus = new Vue({
  data() {
    return ({

    })
  },
  methods: {
    setStore(i, v) {
      utils.setStore(i, v)
    },
    getStore(i) {
      return utils.getStore(i)
    }
  }
})

new Vue({
  el: '#app',
  render: h => h(App),
  router
})
